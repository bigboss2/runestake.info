import {Component, OnInit, OnDestroy} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {HttpClient} from '@angular/common/http';
import BigNumber from 'bignumber.js';
import {forkJoin, Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../Services/api-service.service';
import {StakersAssetData} from '../../models/stakersAssetData';
import {CalculationService} from '../../Services/calculation.service';
import {StakerShareInfo} from '../../models/staker-pool/staker-info';
import {AssetDetail} from '../../models/assetDetail';
import {StakerProfit} from '../../models/staker-pool/staker-profit';
import {LocationStrategy} from '@angular/common';
import {DisplayData} from '../../models/staker-pool/display-data';
import {LocalStorage, StorageMap} from '@ngx-pwa/local-storage';


@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})


export class DemoComponent implements OnInit, OnDestroy {
  constructor(private spinner: NgxSpinnerService,
              private http: HttpClient,
              private activeRoute: ActivatedRoute,
              private apiService: ApiService,
              private calculationService: CalculationService,
              private router: Router,
              private storage: StorageMap) {
  }

  isCollapsed = true;

  isChaosnet = true;
  activeClass = this.isChaosnet ? 'badge-info' : 'badge-neutral';
  address = '';
  unredactedAddresss = '';

  bigNumber: any;
  //
  isShowAlert = false;
  errorMessage = 'Address not found.';

  baseNumber = Math.pow(10, 8); // 1e8

  isRedacted = false;
  hasQuery = false;
  copyButtonTest = 'Save URL';

// new data
  userPools: StakersAssetData[];
  userPoolMap: {};
  generalPoolMap: {};
  eventMap: {};
  poolList: Array<string>;
  stakeShareInfor: StakerShareInfo[];
  stakerOriginalStakeMap = {};
  priceMap: {};
  stakerProfit: StakerProfit[];
  displayData: DisplayData[] = [];
  baseTicker = 'BUSD';

  assetDic = {
    BNB: 'https://chaosnet.bepswap.com/static/media/coin-bnb.25324922.svg',
    FTM: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/FTM-A64/logo.png',
    BUSD: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BUSD-BD1/logo.png',
    RUNE: '../../assets/img/Thorchain.jpg',
    AVA: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/AVA-645/logo.png',
    TWT: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/TWT-8C2/logo.png',
    ETH: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/ETH-1C9/logo.png',
    BTCB: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BTCB-1DE/logo.png',
    CAN: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/CAN-677/logo.png',
    BULL: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BULL-BE4/logo.png',
    DOS: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/DOS-120/logo.png',
    SWINGBY: 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/SWINGBY-888/logo.png'
  };

  isDebugNeedLog = false;

  changeConvert(ticker: string){
    this.baseTicker = ticker; // BUSD, RUNE, ASSET
    this.storage.set('runestake_info_base_ticker', ticker).subscribe(() => {});
    const displayData = this.calculationService.calculateDisplayData(this.stakerProfit, this.getBUSDPrice(), this.baseTicker);
    this.displayData = displayData
        .sort((a, b) =>
            b.totalPricePool.comparedTo(a.totalPricePool));
  }

  demoLogData(data){
    if (this.isDebugNeedLog){
      console.log(data);
    }
  }

  getImage(name) {
    return this.assetDic[name] !== undefined ? this.assetDic[name] : '../../assets/img/gray.png';
  }

  redactToggle(state) {
    this.isRedacted = state;
    if (state) {
      this.unredactedAddresss = this.address;
      this.address = '⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡';
    } else {
      this.address = this.unredactedAddresss;
    }
  }

  showHideError(message, isShow) {
    this.showHideSpinner(true);
    this.isShowAlert = isShow;
    this.errorMessage = message;
  }

  getBaseUrl(): string {
    return this.isChaosnet ? 'https://chaosnet-midgard.bepswap.com' : 'https://midgard.bepswap.com';
  }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('profile-page');
    this.apiService.baseUrl = this.getBaseUrl();
    this.isDebugNeedLog = false;
    // this.isChaosnet = true;
    this.storage.get('runestake_info_base_ticker').subscribe((user: string ) => {
      console.log(user);
      this.baseTicker = user !== undefined ? user : 'BUSD';
    });

    this.activeRoute.queryParams.subscribe(params => {
      // this.isChaosnet = true;
      this.address = params.address;
      //  console.log(this.address);
      if (this.address !== undefined && this.address.length > 20) {
        this.hasQuery = true;
        this.testEvent();
      }else {
        this.hasQuery = false;
        this.address = '';
      }
    });
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('profile-page');
  }

  chaosNetClick(isChaosNet) {
    console.log('clicfg l' + isChaosNet);
    this.address = '';
    this.isChaosnet = isChaosNet;

    this.apiService.baseUrl = this.getBaseUrl();
    console.log('change to url: ' + this.apiService.baseUrl);
  }

  search() {
    if (this.address.length < 10) {
      this.showHideError('Please double check your address ', true);
    } else {
      this.showHideError('', false);
      this.testEvent();
    }

  }

  onKey(event: KeyboardEvent) {
    this.showHideError('', false);
    if (event.key === 'Enter') {
      // this.loadData();
      this.testEvent();
    }
  }

  showHideSpinner(isHide) {
    !isHide ? this.spinner.show() : this.spinner.hide();
  }


  testEvent() {
    if (!this.hasQuery) {
      console.log('prepar routing');
      this.router.navigateByUrl('/demo?address=' + this.address);
      return;
    }else {
      console.log('no routing needed');
    }
    this.showHideSpinner(false);
    this.loadEvent();
  }

  resetLocalData() {
    this.userPools = [];
    this.userPoolMap = {};
    this.generalPoolMap = {};
    this.eventMap = {};
    this.poolList = [];
    this.stakeShareInfor = [];
    this.stakerOriginalStakeMap = {};
    this.stakerProfit = [];
  }

  loadUserPoolAndPool(address: string) {
    this.apiService.getStaker(this.address).subscribe(stakerPool => {
      if (stakerPool.poolsArray === null) {
        console.log('Error getting user pool data.');
        this.showHideError('No pools data', true);
        return;
      }
      this.poolList = stakerPool.poolsArray;

      console.log("calling price " + this.isChaosnet ? 'BNB.BUSD-BD1' : 'BNB.BUSD-BAF');
      forkJoin([
        this.apiService.getStakePools(this.address, stakerPool.poolsArray),
        this.apiService.getPools(stakerPool.poolsArray),
        this.apiService.getPrice([this.isChaosnet ? 'BNB.BUSD-BD1' : 'BNB.BUSD-BAF'])])
          .subscribe(results => {
            //     console.log('get all data');
            //     console.log(results);
            if (results.length === 3) {
              this.userPools = results[0];
              this.userPoolMap = this.userPools.reduce(
                  (dict, el, index) => (dict[el.asset] = el, dict),
                  {}
              );
              this.generalPoolMap = results[1].reduce(
                  (dict, el, index) => (dict[el.asset] = el, dict),
                  {}
              );

              this.priceMap = results[2].reduce(
                  (dict, el, index) => (dict[el.asset] = el, dict),
                  {}
              );
              //   console.log('price map ' + results[2]);
              //   console.log(this.priceMap);
              this.loadUserTransaction(this.address, stakerPool.poolsArray);
            }
          }, error => {
            console.log('Error getting user pool data.');
            this.showHideError('Something went wrong. Please try again.', true);
          });

    }, error => {
      console.log('Error getting user pool data.');
      this.showHideError('Error getting pool data.', true);
    });
  }

  loadUserTransaction(address, asset: string[]) {
    //     console.log('sending asset: ' + asset);
    this.apiService.getAllTranscation(address, 'stake,unstake').subscribe(events => {
      //     console.log('all event map');
      this.eventMap = events.txs.reduce(
          (dict, el, index) => ((dict[el.pool] || (dict[el.pool] = [])).push(el), dict),
          {}
      );
      //     console.log(this.eventMap);
      //     console.log('user pool mage');
      //     console.log(this.userPoolMap);
      //     console.log('general pool map');
      //     console.log(this.generalPoolMap);
      // tslint:disable-next-line:max-line-length
      // @ts-ignore
      this.stakeShareInfor = this.calculationService.calculatePoolData(this.userPoolMap, this.generalPoolMap, this.eventMap, this.poolList);
      // tslint:disable-next-line:max-line-length
      // @ts-ignore
      this.stakerOriginalStakeMap = this.calculationService.calculationTransactionData(this.generalPoolMap, this.eventMap, this.poolList);
      this.createStakerProfit();
    }, error => {
      console.log('Error getting transaction');
      this.showHideError('Error getting transactions history.', true);
    });
  }

  loadEvent() {

    this.resetLocalData();
    this.showHideSpinner(false);
    const limit = 50;
    this.loadUserPoolAndPool(this.address);
  }

  toString(value: BigNumber): string {
    return value.div(this.baseNumber).toFormat(2);
  }
  // profitAsset: string;
  // profitTarget: string;
  // percent: string;
  // priceSharePrice: string;
  // priceOriginal: string;

//   [profitInAsset, profitInRune, totalPercent.toFixed(3),
//   pricePoolShare, priceOriginalStake, priceInProfit
// ];
  createStakerProfit(){
    for (const stakerInfo of this.stakeShareInfor){
      // [asset: BigNumber, target: BigNumber]
      const originalPair = this.getOrginalStake(stakerInfo.pool);
      // [profitAsset: BigNumber, profitTarget: BigNumber, percent: string]
      const profitSum = this.getProfit(stakerInfo, stakerInfo.pool);
      const assetPriceInRune = new BigNumber(stakerInfo.assertPrice);

      // const raitoText = originalPair[2].isEqualTo(0) ? '' : ' - ' + originalPair[2].div(100).toFormat(2) + '% Withdrawn';
      const raitoText = ' - Unstake(%)';

      const profit = {
        pool: stakerInfo.pool,
        asset: stakerInfo.asset,
        shareAsset: stakerInfo.assetShare,
        shareTarget: stakerInfo.targetShare,
        totalAsset: originalPair[0],
        totalTarget: originalPair[1],
        withdrawRatio: raitoText,
        profitAsset: profitSum[0],
        profitTarget: profitSum[1],
        percent: profitSum[2],
        pricePoolShareNum: profitSum[3],
        pricePoolShare: profitSum[4],
        priceOriginalStake: profitSum[5],
        priceInProfit: profitSum[6],
        priceInRune: profitSum[7]
      };

      this.stakerProfit.push(profit);
    }

    this.changeConvert(this.baseTicker);
    this.showHideError('', false);
  }




  getOrginalStake(pool: string): [asset: BigNumber, target: BigNumber, widthrawRatio: BigNumber]{
    const original = this.stakerOriginalStakeMap[pool];
    if (original === undefined) {
      return  [new BigNumber(0), new BigNumber(0), new BigNumber(0)];
    }
    return  [original.finalAsset,
      original.finalTarget, original.totalWithdrawRatio];

  }

  getOriginalAmount(stake: BigNumber, unstake: BigNumber): BigNumber {
    return stake.minus(unstake);
  }


  getProfit(stakerInfor: StakerShareInfo, pool: string):
      [profitAsset: BigNumber, profitTarget: BigNumber, percent: string, pricePoolShare: BigNumber,
        pricePoolShareUSD: string, priceStakeUSD: string, priceProfitUSD: string, assetPriceInRune: BigNumber ] {

    const assetPriceInRune = new BigNumber(stakerInfor.assertPrice);

    const originalPair = this.getOrginalStake(pool);
    this.demoLogData('\n\n\n');
    this.demoLogData('calculation profit');
    this.demoLogData('pool ' + pool + ' final asset: ' + originalPair[0].div(this.baseNumber).toString() +
        ' final rune: ' + originalPair[1].div(this.baseNumber).toString() +
        ' RuneShare: ' + stakerInfor.targetShare.div(this.baseNumber).toString() +
        ' AssetShare: ' + stakerInfor.assetShare.div(this.baseNumber).toString());

    const profitInAsset = stakerInfor.assetShare.minus(originalPair[0]);
    const profitInRune = stakerInfor.targetShare.minus(originalPair[1]);

    this.demoLogData('pool ' + pool + ' profitInAsset: ' + profitInAsset.div(this.baseNumber).toString() +
        ' profitInRune: ' + profitInRune.div(this.baseNumber).toString() );

    const doubleRuneShare = stakerInfor.targetShare.multipliedBy(2);
    const doubleAssetShare = stakerInfor.assetShare.multipliedBy(2);

    const doubleRuneOriginal = originalPair[1].multipliedBy(2);
    const doubleAssetOriginal = originalPair[0].multipliedBy(2);

    this.demoLogData('pool ' + pool + ' doubleRuneShare: ' + doubleRuneShare.div(this.baseNumber).toString() +
        ' doubleAssetShare: ' + doubleAssetShare.div(this.baseNumber).toString() +
        ' doubleRuneOriginal: ' + doubleRuneOriginal.div(this.baseNumber).toString() +
        ' doubleAssetOriginal: ' + doubleAssetOriginal.div(this.baseNumber).toString());


    const runePercent = (doubleRuneShare.minus(doubleRuneOriginal)).div(doubleRuneOriginal);
    const assetPercent = (doubleAssetShare.minus(doubleAssetOriginal)).div(doubleAssetOriginal);
    const totalPercent = (runePercent.plus(assetPercent)).div(2);

    this.demoLogData('pool ' + pool + ' runePercent: ' + runePercent.multipliedBy(100).toNumber() +
        ' assetPercent: ' + assetPercent.multipliedBy(100).toNumber() +
        ' average %: ' + totalPercent.multipliedBy(100));

    const totalShareInRune1 = doubleAssetShare.multipliedBy(assetPriceInRune).plus(doubleRuneShare);
    const totalStakeInRune1 = doubleRuneOriginal.plus(doubleAssetOriginal.multipliedBy(assetPriceInRune));

    const totalShareInAsset1 = doubleRuneShare.div(assetPriceInRune).plus(doubleAssetShare);
    const totalStakeInAsset1 = doubleRuneOriginal.div(assetPriceInRune).plus(doubleAssetOriginal);

    const profitInRune1 = totalShareInRune1.minus(totalStakeInRune1);
    const profitInAsset1 = totalShareInAsset1.minus(totalStakeInAsset1);

    const grossMarginRune = profitInRune1.div(totalStakeInRune1).multipliedBy(100) ;
    const grossMarginAsset = profitInAsset1.div(totalStakeInAsset1).multipliedBy(100) ;
    this.demoLogData('real grossMarginRune ' + grossMarginRune.toNumber().toFixed(2));
    this.demoLogData('real grossMarginAsset ' + grossMarginAsset.toNumber().toFixed(2));

    const finalGrossPercent = (grossMarginAsset.plus(grossMarginRune)).div(2);
    this.demoLogData('final grossMarget % ' + finalGrossPercent.toNumber().toFixed(2));
    const totalProfile =  profitInAsset.multipliedBy(assetPriceInRune).plus(profitInRune);

    // const grossMargin = profitInRune.div(totalStakeInRune).multipliedBy(100) ;

    // gross_margin = 100 * profit / revenue



    const pricePoolShare = this.getPrice(assetPriceInRune, stakerInfor.assetShare, stakerInfor.targetShare);
    const priceOriginalStake = this.getPrice(assetPriceInRune, originalPair[0], originalPair[1]);
    const priceInProfit = this.getPrice(assetPriceInRune, profitInAsset, profitInRune);

    const totalShareInRune = stakerInfor.assetShare.multipliedBy(assetPriceInRune).plus(stakerInfor.targetShare);
    const priceIntotalShareInRune = this.getPrice(assetPriceInRune, (new BigNumber(0)), totalShareInRune);
    // const totalStakeInRune = originalPair[1].plus(originalPair[0].multipliedBy(assetPriceInRune));

    this.demoLogData('pool ' + pool + ' pricePoolShare: ' + this.toString(pricePoolShare)
        +  ' priceOriginalStake: ' + this.toString(priceOriginalStake)
        +   ' priceInProfit: ' + this.toString(priceInProfit)
    );
    //this.demoLogData('compareto');
    //this.demoLogData('pool ' + pool + ' pricePoolShare: ' + (stakerInfor.assetShare.multipliedBy( 23.856
    //     )).plus((stakerInfor.targetShare.multipliedBy(0.3793))).div(this.baseNumber).toString()
    //     +  ' priceOriginalStake: ' +  ( originalPair[0].multipliedBy( 23.856
    //     )).plus(( originalPair[1].multipliedBy(0.3793))).div(this.baseNumber).toString()
    //     +   ' priceInProfit: ' +  ( profitInAsset.multipliedBy( 23.856
    //     )).plus(( profitInRune.multipliedBy(0.3793))).div(this.baseNumber).toString()
    // );
    // pricePoolShareUSD: BigNumber, priceStakeUSD: BigNumber, priceProfitUSD: BigNumber
    return [profitInAsset, profitInRune, finalGrossPercent.toFixed(3),
      pricePoolShare, this.toString(pricePoolShare), this.toString(priceOriginalStake), this.toString(priceInProfit),
      assetPriceInRune
    ];
    // return [profitInAsset, profitInRune, grossMargin.toNumber().toFixed(2), totalShareInRune, totalStakeInRune, totalProfile ];


  }

  getPrice(assetPriceInRune: BigNumber, amount: BigNumber, runeAmount: BigNumber): BigNumber{
    const priceINBUSD = this.getBUSDPrice();
    const priceInRUNE = (new BigNumber(1)).div(priceINBUSD);
    const priceInASSET = assetPriceInRune.multipliedBy(priceInRUNE);

    const targetPrice = runeAmount.multipliedBy(priceInRUNE);
    const assetPrice = amount.multipliedBy(priceInASSET);
    //    this.demoLogData('priceINBUSD ' + priceINBUSD);
    //    this.demoLogData('priceInRUNE ' + priceInRUNE.toNumber());
    //    this.demoLogData('priceInASSET ' + priceInASSET.toNumber());

    return targetPrice.plus(assetPrice);

  }

  getPercent(profit: number, current: number, original: number, prefix: string, price: number){
    const grossMargin = profit * 100 / original ;
    // const

    //    this.demoLogData('totalShareIn ' + prefix + '  ' + current + '  ' + '$' + current * price);
    //    this.demoLogData('totalStakeIn ' + prefix + '  ' + original + '  ' + '$' + original * price);
    //    this.demoLogData('profit ' + profit + '  ' + '$' + profit * price );
    //    this.demoLogData('grossMargin ' + grossMargin.toFixed(2) + '%');
  }

//     So the percentage should be
//     A: calculate the current pool share amounts
//     B: calculate the staked amounts
// % = (A - B) / A

  // 10 + 1 * (10 priceOfRune) = 20 rune

  getClassProfit(percent = '') {
    //this.demoLogData('percenttage class ' + percent.length);
    if (percent.length === undefined) {
      return 'text-success';
    }else {
      return percent.indexOf('-') ? 'text-success' : 'text-danger';
    }

  }

  getBUSDPrice(): BigNumber {
    const price: AssetDetail = this.priceMap[this.isChaosnet ? 'BNB.BUSD-BD1' : 'BNB.BUSD-BAF'];
    if (price === undefined) {
      //    this.demoLogData('error get price');
      return new BigNumber(0);
    }else {
      //    this.demoLogData('original price ' + price.priceRune);
      return new BigNumber(price.priceRune);
    }

  }

  onCopiedSuccess(event){
    //    this.demoLogData(event);
    if (event.isSuccess) {
      this.copyButtonTest = 'Address Copied.';
    }else {
      this.copyButtonTest = 'Unable to copied.';
    }
    setTimeout(() => {
      this.copyButtonTest = 'Save URL';
    }, 20000 / 60);
  }
  getDemoAddressURL(): string{
    return 'https://runestake.info/demo?address=' +  this.address;
  }

}
