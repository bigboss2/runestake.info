import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {StakerPool} from '../models/staker-pool/staker-pool.module';
import {StakerTxDetail, StakerTxState} from '../models/Asgard-consumer-model/StakerTxDetail';
import {StakerTransaction} from '../models/staker-pool/staker-transaction';
import {environment} from '../../environments/environment';
import {WeeklyState, WeeklyStateList} from '../models/Asgard-consumer-model/WeeklyStateList';
import {StakersAssetData} from '../models/stakersAssetData';
import {PoolDetail} from '../models/poolDetail';
import {PoolStakeAmount, StakePoolSummary} from '../models/staker-pool/StakePoolSummary';
import {StakerHistoryPool} from '../models/StakerHistoryPool';
import BigNumber from 'bignumber.js';
import {map} from 'rxjs/operators';
import {AssetDetail} from '../models/assetDetail';

@Injectable({
  providedIn: 'root'
})
export class AsgardConsumerApiService {

  constructor(private http: HttpClient) {
  }

  // baseUrl = 'https://asgard-consumer-git-development.suresdey.vercel.app';
    baseUrl = environment.ASGARD_BASE_URL;

  apiAsgardConsumerApiServiceLog(data){
    if (!environment.production){
      console.log(data);
    }
  }

  isProdENV(){
      return environment.ASGARD_BASE_URL;
  }

    getPools(pools: string[]): Observable<PoolDetail[]> {
        const params = new HttpParams()
            .set('pool', pools.join(','));
        return this.http.get<PoolDetail[]>(this.baseUrl + '/api/v1/lastPool', {params});
    }

  getStakerTxDetail(address: string, pool: string): Observable<StakerTxState> {
    this.apiAsgardConsumerApiServiceLog(this.baseUrl);
    return this.http.get<StakerTxState>(`${this.baseUrl}/api/calculator?address=${address}&pool=${pool}`);
  }

    getStakerResetTxDetail(address: string, pool: string): Observable<StakerTxState> {
        this.apiAsgardConsumerApiServiceLog(this.baseUrl);
        return this.http.get<StakerTxState>(`${this.baseUrl}/api/v1/staker/reset?address=${address}&pool=${pool}`);
    }

    getStakerHistorycalPool(address: string, pool: string, isReset: boolean): Observable<StakePoolSummary> {
        const endpoint = !isReset ? `history/liquidity` : 'staker/reset';
        const isDev = !environment.production ? '&isDev' : '';
        this.apiAsgardConsumerApiServiceLog(this.baseUrl);
        return this.http.get<StakerHistoryPool>(`${this.baseUrl}/api/v2/${endpoint}?address=${address}&pools=${pool}${isDev}`)
            .pipe(map(value => this.createSummaryPool(value)));
    }

    createSummaryPool(data: StakerHistoryPool): StakePoolSummary {
        const txHis: StakerHistoryPool = data;
        const stake: PoolStakeAmount = {
            asset: txHis.pool,
            assetAmount: new BigNumber(txHis.assetstake),
            targetAmount: new BigNumber(txHis.runestake),
            totalAssetAmount: new BigNumber(txHis.totalstakedasset),
            totalTargetAmount: new BigNumber(txHis.totalstakedrune),
            totalBUSDAmount: new BigNumber(txHis.totalstakedusd),
            unit: new BigNumber(txHis.poolunits),
        };

        const unStake: PoolStakeAmount = {
            asset: txHis.pool,
            assetAmount: new BigNumber(txHis.assetwithdrawn),
            targetAmount: new BigNumber(txHis.runewithdrawn),
            totalAssetAmount: new BigNumber(txHis.totalunstakedasset),
            totalTargetAmount: new BigNumber(txHis.totalunstakedrune),
            totalBUSDAmount: new BigNumber(txHis.totalunstakedusd),
            unit: new BigNumber(txHis.poolunits),
        };

        this.apiAsgardConsumerApiServiceLog('totalAssetAmount ' + stake.totalAssetAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('totalTargetAmount ' + stake.totalTargetAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('totalBUSDAmount ' + stake.totalBUSDAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('assetAmount ' + stake.assetAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('targetAmount ' + stake.targetAmount.div(Math.pow(10, 8)).toFixed(2));

        this.apiAsgardConsumerApiServiceLog('########');
        this.apiAsgardConsumerApiServiceLog('totalAssetunStake ' + unStake.totalAssetAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('totalTargetunStake ' + unStake.totalTargetAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('########');
        this.apiAsgardConsumerApiServiceLog('totalBUSDAmountunStake ' + unStake.totalBUSDAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('assetAmountunStake ' + unStake.assetAmount.div(Math.pow(10, 8)).toFixed(2));
        this.apiAsgardConsumerApiServiceLog('targetAmountunStake ' + unStake.targetAmount.div(Math.pow(10, 8)).toFixed(2));

        const summary: StakePoolSummary = {
            asset: txHis.pool,
            stake,
            withdraw: unStake,
            time: `${txHis.firststake}`
        };
        return summary;
    }


    getStakePoolSummary(address: string, pools: string[], isReset: boolean = false): Observable<any>{
        const pages = [];
        for (const pool of pools){
            const page = this.getStakerHistorycalPool(address, pool, isReset);
            pages.push(page);
        }
        return forkJoin(pages);
  }

  getAllStakerTxDetail(address: string, pools: string[], isReset: boolean = false): Observable<{[pool: string]: StakerTxDetail[] }>{
    const transactions = new Observable(subscriber => {

      const pages = [];
      for (const pool of pools){
          if (isReset){
              const page = this.getStakerResetTxDetail(address, pool);
              pages.push(page);
          }else {
              const page = this.getStakerTxDetail(address, pool);
              pages.push(page);
          }

      }
      forkJoin(pages).subscribe(results => {
            this.apiAsgardConsumerApiServiceLog(results);
            const txs: {[pool: string]: StakerTxDetail[] } = {};

            results.forEach(value => {
                this.apiAsgardConsumerApiServiceLog(value);
                // const state: StakerTxState = value;
                // @ts-ignore
                txs[value.pool] = value.txDetail;
            });
            subscriber.next(txs);
          },
          error => {
            subscriber.error(error);
          });
    });
    // @ts-ignore
    return transactions;
  }

    getWeeklyState(address: string, pool: string, isReset: boolean): Observable<WeeklyStateList> {
        this.apiAsgardConsumerApiServiceLog(this.baseUrl);
        const preset = isReset ? '&reset' : '';
        return this.http.get<WeeklyStateList>(`${this.baseUrl}/api/weekly?address=${address}&pool=${pool}${preset}`);
    }


    getAllWeeklyState(address: string, pools: string[], isReset: boolean): Observable<{[pool: string]: WeeklyState[] }>{
        const transactions = new Observable(subscriber => {

            const pages = [];
            for (const pool of pools){
                const page = this.getWeeklyState(address, pool, isReset);
                pages.push(page);
            }
            forkJoin(pages).subscribe(results => {
                    this.apiAsgardConsumerApiServiceLog(results);
                    const txs: {[pool: string]: WeeklyState[] } = {};

                    results.forEach(value => {
                        this.apiAsgardConsumerApiServiceLog(value);
                        // const state: StakerTxState = value;
                        // @ts-ignore
                        txs[value.pool] = value.data.reverse();
                    });
                    subscriber.next(txs);
                },
                error => {
                    subscriber.error(error);
                });
        });
        // @ts-ignore
        return transactions;
    }

    getStakePools(address: string, pools: string[]): Observable<StakersAssetData[]> {
        const params = new HttpParams()
            .set('pools', pools.join(','))
            .set('address', address);
        return this.http.get<StakersAssetData[]>(`${this.baseUrl}/api/v2/staker/pool`, {params});

    }

    getPrice(pools: string[]): Observable<AssetDetail[]>{
        // const pool = ['BUSD-BAF'];
        const params = new HttpParams()
            .set('asset', pools.join(','));
        return  this.http.get<AssetDetail[]>(this.baseUrl + '/api/v1/asset/price', {params});
    }

    logUsage(type: string): Observable<void>{
        // const pool = ['BUSD-BAF'];
        const params = new HttpParams()
            .set('type', type)
            .set('agent', 'web');
        return this.http.get<void>(this.baseUrl + '/api/v1/log', {params});
    }


}
