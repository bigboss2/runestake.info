import { Injectable } from '@angular/core';
import {ApiService} from './api-service.service';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AssetDetail} from '../models/assetDetail';
import BigNumber from 'bignumber.js';
import {environment} from '../../environments/environment';
import {AsgardConsumerApiService} from './asgard-consumer-api.service';

@Injectable({
  providedIn: 'root'
})
export class PricesService {
  priceMap: {};
  constructor() { }
  asgardService: AsgardConsumerApiService;
  apiService: ApiService;

  getPrice(pools: string[]): Observable<boolean>{
   return  this.apiService.getPrice(pools)
        .pipe(tap(data => {
          this.priceMap = data.reduce(
              (dict, el, index) => (dict[el.asset] = el, dict),
              {}
          );
        //  console.log('getPrice is working');
        //  console.log(this.priceMap);
        }),
            map(value => true));
  }

  logData(data){
    if (!environment.production){
      console.log(data);
    }
  }

  getBUSDPrice(): BigNumber {
    return this.getAssetPrice('BNB.BUSD-BD1');
  }

  getAssetPrice(asset: string): BigNumber {
    //  this.demoLogData('get asset price ' + asset);

    const price: AssetDetail = this.priceMap[asset];
//    this.demoLogData('get asset price ' + price.priceRune);
    if (price === undefined) {
      //    this.demoLogData('error get price');
      return new BigNumber(0);
    }else {
      //    this.demoLogData('original price ' + price.priceRune);
      return new BigNumber(price.priceRune);
    }
  }

  getPriceBN(pool: string){
    const busd = this.getBUSDPrice();
    const runeInBUSD = new BigNumber(1).div(busd);
    const asset = this.getAssetPrice(pool);

    if (pool === 'RUNE'){
      this.logData('price of asset ' + runeInBUSD.toFixed(2));
      return runeInBUSD;
    }else {
      this.logData('price of asset ' + asset.multipliedBy(runeInBUSD).toFixed(2));
      return asset.multipliedBy(runeInBUSD);
    }
  }

  computePrice(isTarget: boolean, pool: string, baseTicker: string): string{
    const busd = this.getBUSDPrice();
    const runeInBUSD = new BigNumber(1).div(busd);
    const asset = this.getAssetPrice(pool);
    switch (baseTicker) {
      case 'RUNE':
        if (isTarget){
          return 'ᚱ1' ;
        }else {
          // return '1';
          return 'ᚱ' + asset.toFixed(3) ;
        }
        break;
      case 'ASSET':
        if (isTarget){
          return this.toString(new BigNumber(1).div(asset), pool, baseTicker ) ;
        }else {
          return '1';
        }
        break;
      case 'BUSD':
        if (isTarget){
          return '$' + runeInBUSD.toFixed(2) ;
        }else {
          return '$' + asset.multipliedBy(runeInBUSD).toFixed(2) ;
        }
        break;
    }
  }

  toString(value: BigNumber, pool: string = '', baseTicker: string): string {
    let decimal = 3;
    if ((pool === 'BNB.BULL-BE4') || (pool === 'BNB.BTCB-1DE')){
      decimal = 5;
    }
    return value.div(environment.BNB_BASE_NUMBER).toFormat(decimal);
  }

}
