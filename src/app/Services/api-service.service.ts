import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {StakerPool} from '../models/staker-pool/staker-pool.module';
import {InlineResponse2001} from '../models/inlineResponse2001';
import {TxDetails} from '../models/txDetails';
import {StakersAssetData} from '../models/stakersAssetData';
import {PoolDetail} from '../models/poolDetail';
import {AssetDetail} from '../models/assetDetail';
import {StakerTransaction, StakerTransactionList} from '../models/staker-pool/staker-transaction';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) {
    }

    baseUrl = '';

    apiServiceLogData(data){
        if (!environment.production){
            console.log(data);
        }
    }

    getStaker(address: string): Observable<StakerPool> {
        this.apiServiceLogData(this.baseUrl);

        return this.http.get<StakerPool>(this.baseUrl + '/v1/stakers/' + address);
    }

    getStakePools(address: string, pools: string[]): Observable<StakersAssetData[]> {
        const params = new HttpParams()
            .set('asset', pools.join(','));
        return this.http.get<StakersAssetData[]>(this.baseUrl + '/v1/stakers/' + address + '/pools', {params});

    }

    getPools(pools: string[]): Observable<PoolDetail[]> {
        const params = new HttpParams()
            .set('asset', pools.join(','))
            .set('view', 'simple');
        return this.http.get<PoolDetail[]>(this.baseUrl + '/v1/pools/detail', {params});
    }

    getPrice(pools: string[]): Observable<AssetDetail[]>{
        // const pool = ['BUSD-BAF'];
        this.apiServiceLogData('calling price ' + pools);
        const params = new HttpParams()
            .set('asset', pools.join(','));
        return  this.http.get<AssetDetail[]>(this.baseUrl + '/v1/assets', {params});
    }

    getStakeTranscations(address: string, pools: string[]): Observable<{string: StakerTransaction[] }>{
        const transactions = new Observable(subscriber => {

            const morePages = [];
            for (const pool of pools){
                const netPages = this.getStakeEvent(address, pool);
                morePages.push(netPages);
            }

            forkJoin(morePages).subscribe(results => {
                    this.apiServiceLogData(results);
                    const txs = {};

                    for (const tx of results){
                        // @ts-ignore
                        this.apiServiceLogData(tx.data);
                        // @ts-ignore
                        txs[tx.pool] = tx.data;
                }

                    this.apiServiceLogData(txs);
                    subscriber.next(txs);
                },
                error => {
                    subscriber.error(error);
                });
        });
        // @ts-ignore
        return transactions;
    }



    getAllTranscation(address: string, type: string = 'stake,unstake'): Observable<InlineResponse2001> {
        const transaction = new Observable(subscriber => {

            const limit = 30;
            this.getStakerTransaction(address, 0, 1, type).subscribe(txs => {
                    const count = txs.count;
                    this.apiServiceLogData('All transaction count: ' + count);
                    const numberOfpage = Math.ceil(count / limit);
                    this.apiServiceLogData(txs);
                    const morePages = [];
                    for (let i = 0; i <= numberOfpage - 1; i++) {
                        const netPages = this.getStakerTransaction(address, i * limit, limit, type);
                        morePages.push(netPages);
                    }
                    forkJoin(morePages).subscribe(results => {
                            const events = this.concatEvent(results);
                            if (count === events.length) {
                                const final: InlineResponse2001 = {count, txs: events};
                                this.apiServiceLogData(final);
                                subscriber.next(final);
                            } else {
                                subscriber.error('Cannot fetch all transactions. Please try again.');
                            }
                        },
                        error => {
                            subscriber.error(error);
                        });
                }
                , error => {
                    subscriber.error(error);
                });

        });
        return transaction;
    }

    concatEvent(txs: Array<InlineResponse2001>): Array<TxDetails> {
        let result: InlineResponse2001;
        let events: Array<TxDetails> = [];
        for (result of txs) {
            events = events.concat(result.txs);
        }
        return events;
    }

    // tslint:disable-next-line:max-line-length
    getStakerTransaction(address: string, offset: number, limit: number, type: string = 'stake,unstake', asset: string[] = []): Observable<InlineResponse2001> {
        const params = new HttpParams()
            .set('address', address)
            .set('offset', String(offset))
            .set('limit', String(limit))
            .set('type', type);
        return this.http.get<InlineResponse2001>(this.baseUrl + '/v1/txs', {params});
    }

    getStakeEvent(address: string, pool: string): Observable<StakerTransactionList> {
        const params = new HttpParams()
            .set('address', address)
            .set('pool', pool);
        return this.http.get<StakerTransactionList>('http://localhost:3001/api/staker', {params});
    }
}
