import { Injectable } from '@angular/core';
import BigNumber from 'bignumber.js';
import {StakersAssetData} from '../models/stakersAssetData';
import {PoolDetail} from '../models/poolDetail';
import {TxDetails} from '../models/txDetails';
import {StakerShareInfo} from '../models/staker-pool/staker-info';
import {StakerTransaction} from '../models/staker-pool/staker-transaction';
import {StakerOriginal} from '../models/staker-pool/staker-original';
import {StakerProfit} from '../models/staker-pool/staker-profit';
import {DisplayData} from '../models/staker-pool/display-data';
import {PoolStakeAmount, Profit, StakePoolSummary} from '../models/staker-pool/StakePoolSummary';
import {Price, StakerTxDetail} from '../models/Asgard-consumer-model/StakerTxDetail';
import {environment} from '../../environments/environment';
import {UtilsService} from './utils.service';
@Injectable({
  providedIn: 'root'
})

export class CalculationService {

  constructor(private utils: UtilsService) {
  }

  baseNumber = Math.pow(10, 8); // 1e8

  calculationServiceLogData(data){
    if (!environment.production){
      console.log(data);
    }
  }

  calculatePoolData(userPool: {Asset: StakersAssetData},
                    poolsMap: {Asset: PoolDetail},
                    userTransactionMap: {Asset: [TxDetails]},
                    poolList: Array<string>): StakerShareInfo[]{

    const stakeInfors: StakerShareInfo[] = [];
    for (const [key, val] of Object.entries(userPool)) {
      // this.calculationServiceLogData(key, val);
      const eachPool = poolsMap[key];
     //     conle.log('eachpool ' + eachPool);

      const poolAsset = eachPool.asset;

      const stakeUnitsBN = new BigNumber(val.units);
      const poolShares = this.calculateShare(stakeUnitsBN, eachPool);
      // const price = new BigNumber(poolAsset.price);
      const price = eachPool.price;
     //     conle.log(eachPool);
     //     conle.log('prince in pool ' + price);
      const object = {
        pool: key,
        assertPrice: price,
        asset: this.utils.getAssetName(poolAsset),
        target: 'RUNE',
        stakeUnit: stakeUnitsBN,
        assetShare: poolShares[1],
        targetShare: poolShares[0]
      };
      stakeInfors.push(object);

    }
    return stakeInfors;

  }

  calculationTransactionData(poolsMap: {Asset: PoolDetail},
                             userTransactionMap: {Asset: [TxDetails]},
                             poolList: Array<string>): {}{
   //     conle.log('userTransactionMap ' + userTransactionMap);
    const transactions: StakerTransaction[] = [];

   //     conle.log('user poold list +' + poolList);
    for (const [key, val] of Object.entries(userTransactionMap)) {
     //     conle.log('user poold key +' + key);
     //     conle.log('index +' + poolList.indexOf(key));
      if (poolList.indexOf(key) >= 0){
        // const eachPool = poolsMap[key];
        for (const txDetail of val){
   //     this.calculationServiceLogData('each tx');
   //     this.calculationServiceLogData(txDetail);
          if (txDetail.type === TxDetails.TypeEnum.Stake) {
            transactions.push(this.getStakeTransaction(txDetail));
          }else if (txDetail.type === TxDetails.TypeEnum.Unstake) {
            transactions.push(this.getUnstakeTransaction(txDetail));
          }else {
     //     this.calculationServiceLogData('error getting tx type');
          }
        }
      }else {
 //     this.calculationServiceLogData('this pool not activate');
      }

    }
    this.calculationServiceLogData('\n\n');
    for (const tx of transactions){
      this.calculationServiceLogData('pool  ' + tx.pool + ' ' + tx.type.toString() + ' ' +
                  ' asset amount: ' + tx.assetAmount.div(this.baseNumber).toString() +
       ' rune amount: ' + tx.targetAmount.div(this.baseNumber).toString() +
      ' memo ' + this.extractMemo(tx.memo).toString());
    }

    const transactionsMap =  transactions.reduce(
        (dict, el, index) => ((dict[el.pool] || (dict[el.pool] = [])).push(el), dict),
        {}
    );
   //     conle.log('transaction Map');
   //     conle.log(transactionsMap);
    return  this.getTotalTransaction(transactionsMap);
  }

  getTotalTransaction(transactionMap: {}): {}{

    const stakeOriginal = {};

    for (const [key, val] of Object.entries(transactionMap)) {
      let totalAssetStake = new BigNumber(0);
      let totalTargetStake = new BigNumber(0);
      let totalAssetUnstake = new BigNumber(0);
      let totalTargetUnstake = new BigNumber(0);
      let combineUnstakeRatio = new BigNumber(0);
      // @ts-ignore
      const transactions: StakerTransaction[] = val;

      this.calculationServiceLogData('\n\n Start Calculation:');
      for (const transaction of transactions.reverse()){
     // this.calculationServiceLogData(transaction);
        this.calculationServiceLogData(transaction.type + '  : ' + transaction.status + '  :' + new Date(transaction.date * 1000).toLocaleString());
        if (transaction.status !== 'Pending'){
        //  this.calculationServiceLogData('prcess with sucess status');
          if ( transaction.type === 'stake'){
            totalAssetStake = totalAssetStake.plus(transaction.assetAmount);
            totalTargetStake = totalTargetStake.plus(transaction.targetAmount);
            this.calculationServiceLogData(transaction.pool + ' stake  ASSET: ' + transaction.assetAmount.div(this.baseNumber).toString()
                + '  RUNE ' + transaction.targetAmount.div(this.baseNumber).toString() );
            this.calculationServiceLogData(transaction.pool + ' Current balance asset ' + totalAssetStake.div(this.baseNumber).toString()
                + '  RUNE ' + totalTargetStake.div(this.baseNumber).toString() );
          } else {
            totalAssetUnstake = totalAssetUnstake.plus(transaction.assetAmount);
            totalTargetUnstake = totalTargetUnstake.plus(transaction.targetAmount);
            const raito = this.extractMemo(transaction.memo);
            if (raito.isGreaterThan(0)){
              this.calculationServiceLogData(transaction.pool + ' removing ' + raito.div(100) + '% frome asset ' + totalAssetStake.div(this.baseNumber).toString()
                  + '  RUNE ' + totalTargetStake.div(this.baseNumber).toString() );
              totalAssetStake = totalAssetStake.minus(totalAssetStake.multipliedBy(raito.div(10000)));
              totalTargetStake = totalTargetStake.minus(totalTargetStake.multipliedBy(raito.div(10000)));
              this.calculationServiceLogData(transaction.pool + 'Current balance: asset' + totalAssetStake.div(this.baseNumber).toString()
                  + '  RUNE ' + totalTargetStake.div(this.baseNumber).toString() );
            }
            combineUnstakeRatio = combineUnstakeRatio.plus(raito);
          }
        }
      }

      const finnalAsset = totalAssetStake;
      const finalTarget = totalTargetStake;
      this.calculationServiceLogData('\n' + key + ' finnalAsset ' + finnalAsset.div(this.baseNumber).toString()
          + '  finalTarget ' + finalTarget.div(this.baseNumber).toString() );

      const original: StakerOriginal = {
        pool: key,
        totalAssetStake,
        totalTargetStake,
        totalAssetUnstake,
        totalTargetUnstake,
        totalWithdrawRatio: combineUnstakeRatio,
        finalAsset: finnalAsset,
        finalTarget
      };

      stakeOriginal[key] = original;
      this.calculationServiceLogData('\n\n Pool' + key + '  totalAssetStake ' + totalAssetStake.div(this.baseNumber).toFormat(2) +
          '  totalRUNEStake ' + totalTargetStake.div(this.baseNumber).toFormat(2) +
      '  totalAssetUnstake ' + totalAssetUnstake.div(this.baseNumber).toFormat(2) +
      '  totalRUNEtUnStake ' + totalTargetUnstake.div(this.baseNumber).toFormat(2) +
          '\n  WithdrawRatio ' + combineUnstakeRatio +
          '  WithdrawRatio/10000 ' + combineUnstakeRatio.div(10000).toFormat(2) +
       ' final AssetStake: ' + finnalAsset.div(this.baseNumber).toString() +
          ' final RUNEStake: ' + finalTarget.div(this.baseNumber).toString());
    }
    return  stakeOriginal;

  }

  getStakeTransaction(txDetail: TxDetails): StakerTransaction{
    const pool = txDetail.pool;
    const type = txDetail.type.toString();
    const stakeUnit = new BigNumber(txDetail.events.stakeUnits);
    const memo = txDetail.in.memo;
    const status = txDetail.status.toString();
    const date = txDetail.date;
    let asset: string;
    let target: string;
    let assetAmount = new BigNumber(0);
    let targetAmount = new BigNumber(0);
    for (const coin of txDetail.in.coins){
      if (coin.asset !== 'BNB.RUNE-B1A'){
        asset = coin.asset;
        assetAmount = new BigNumber(coin.amount);
      }else {
        target = coin.asset;
        targetAmount = new BigNumber(coin.amount);
      }
    }
    return {  pool, asset, target, stakeUnit, assetAmount, targetAmount, type, memo, status, date  };
  }

  getUnstakeTransaction(txDetail: TxDetails): StakerTransaction{
   //     conle.log(txDetail);
    const pool = txDetail.pool;
    const type = txDetail.type.toString();
    const stakeUnit = new BigNumber(txDetail.events.stakeUnits);
    const memo = txDetail.in.memo;
    const status = txDetail.status.toString();
    const date = txDetail.date;
    let asset: string;
    let target: string;
    let assetAmount = new BigNumber(0);
    let targetAmount = new BigNumber(0);

    for (const out of txDetail.out){
      const coin = out.coins[0];
      if (coin !== undefined){
        if (coin.asset !== 'BNB.RUNE-B1A'){
          asset = coin.asset;
          assetAmount = new BigNumber(coin.amount);
        }else {
          target = coin.asset;
          targetAmount = new BigNumber(coin.amount);
        }
      }
    }



    return {  pool, asset, target, stakeUnit, assetAmount, targetAmount, type, memo, status, date };
  }

  printStakeInfo(stakeInfo: StakerShareInfo){
        this.calculationServiceLogData(
        stakeInfo.asset + '   stake unit:' + stakeInfo.stakeUnit.div(this.baseNumber).toFormat(2)
        + '   runes share' + stakeInfo.targetShare.div(this.baseNumber).toFormat(2)
        + '   runes share' + stakeInfo.targetShare.div(this.baseNumber).toFormat(2)
        + '   asset share' + stakeInfo.assetShare.div(this.baseNumber).toFormat(2)
    );
  }

  extractMemo(memo: string): BigNumber{
    const data  = memo.split(':');
    const withdraw = data[2];
    return withdraw === undefined ? new BigNumber(0) : new BigNumber(withdraw);
  }


  calculateDisplayData(stakeProfits: StakerProfit[], bUSDPrice: BigNumber, baseTicket: string): DisplayData[]{
        // const baseTicket = 'ASSET'; // BUSD, RUNE, ASSET
        const displayArray: DisplayData[] = [];

        for (const stakerProfit of stakeProfits ){
          const profitDetail: StakerProfit = stakerProfit;
          let price: [pricePool: BigNumber, priceStake: BigNumber, priceProfit: BigNumber];
          if (baseTicket === 'BUSD'){
            price = this.calculateUSDValue(stakerProfit, bUSDPrice);
          }else if (baseTicket === 'RUNE'){
            price = this.calculateRuneValue(stakerProfit, bUSDPrice);
          }else if (baseTicket === 'ASSET'){
            price = this.calculateAssetValue(stakerProfit, bUSDPrice);
          }
          const display: DisplayData = {
            pool: stakerProfit.pool,
            asset: stakerProfit.asset,
            shareAsset: stakerProfit.shareAsset,
            shareTarget: stakerProfit.shareTarget,
            totalAsset: stakerProfit.totalAsset,
            totalTarget: stakerProfit.totalTarget,
            profitAsset: stakerProfit.profitAsset,
            profitTarget: stakerProfit.profitTarget,
            percent: stakerProfit.percent,
            totalPricePool: price[0],
            totalPriceStake: price[1],
            totalPriceProfit: price[2],
            baseTicket: (baseTicket === 'ASSET') ? this.trimTicker(stakerProfit.asset) : baseTicket
          };
          displayArray.push(display);
        }
        return displayArray;
  }


  calculateUSDValue(stakeProfit: StakerProfit, bUSDPrice: BigNumber): [pricePool: BigNumber, priceStake: BigNumber, priceProfit: BigNumber]{
    this.calculationServiceLogData('prepare calcuate USD');
  //  this.calculationServiceLogData(new BigNumber(0), new BigNumber(0),new BigNumber(0));

    const priceINBUSD = bUSDPrice;
    const priceInRUNE = (new BigNumber(1)).div(priceINBUSD);
    const priceInASSET = stakeProfit.priceInRune.multipliedBy(priceInRUNE);

    const shareTargetPrice = stakeProfit.shareTarget.multipliedBy(priceInRUNE);
    const shareAssetPrice = stakeProfit.shareAsset.multipliedBy(priceInASSET);
    const totalSharePrice = shareTargetPrice.plus(shareAssetPrice);

    const stakeTargetPrice = stakeProfit.totalTarget.multipliedBy(priceInRUNE);
    const stakeAssetPrice = stakeProfit.totalAsset.multipliedBy(priceInASSET);
    const totalStakePrice = stakeTargetPrice.plus(stakeAssetPrice);

    const profitTargetPrice = stakeProfit.profitTarget.multipliedBy(priceInRUNE);
    const profitAssetPrice = stakeProfit.profitAsset.multipliedBy(priceInASSET);
    const totalProfitPrice = profitTargetPrice.plus(profitAssetPrice);

    return [totalSharePrice, totalStakePrice, totalProfitPrice];

  }

  calculateRuneValue(stakeProfit: StakerProfit, bUSDPrice: BigNumber): [pricePool: BigNumber, priceStake: BigNumber, priceProfit: BigNumber]{

    const assetPriceInRune = stakeProfit.priceInRune;

    const totalStakePrice = stakeProfit.totalAsset.multipliedBy(assetPriceInRune).plus(stakeProfit.totalTarget);

    const totalSharePrice = stakeProfit.shareAsset.multipliedBy(assetPriceInRune).plus(stakeProfit.shareTarget);

    const totalProfitPrice = stakeProfit.profitAsset.multipliedBy(assetPriceInRune).plus(stakeProfit.profitTarget);

    return [totalSharePrice, totalStakePrice, totalProfitPrice];
  }

  calculateAssetValue(stakeProfit: StakerProfit, bUSDPrice: BigNumber): [pricePool: BigNumber, priceStake: BigNumber, priceProfit: BigNumber]{
    const assetPriceInRune = stakeProfit.priceInRune;
    const totalStakePrice = stakeProfit.totalTarget.div(assetPriceInRune).plus(stakeProfit.totalAsset);
    const totalSharePrice = stakeProfit.shareTarget.div(assetPriceInRune).plus(stakeProfit.shareAsset);
    const totalProfitPrice = stakeProfit.profitTarget.div(assetPriceInRune).plus(stakeProfit.profitAsset);
    return [totalSharePrice, totalStakePrice, totalProfitPrice];
  }

  trimTicker(ticker: string): string{
    if (ticker.length > 6) {
      return ticker.substr(0, 5) + '.';
    }else {
      return  ticker;
    }
  }

  calculateHistoricalSummary(stakerTxDetail: StakerTxDetail[], price: BigNumber): StakePoolSummary {
    let totalAssetStake = new BigNumber(0);
    let totalTargetStake = new BigNumber(0);
    let totalConvertedTarget = new BigNumber(0);
    let totalConvertedAsset = new BigNumber(0);
    let totalConvertedUSD = new BigNumber(0);

    let totalAssetUnstake = new BigNumber(0);
    let totalTargetUnstake = new BigNumber(0);
    let totalConvertedTargetUnstake = new BigNumber(0);
    let totalConvertedAssetUnstake = new BigNumber(0);
    let totalConvertedUSDUnstake = new BigNumber(0);

    let asset = '';
    let time: string;

    this.calculationServiceLogData('\n\n Start Calculation: Finding User Original stake');
    for (const txDetail of stakerTxDetail){

      const transaction = txDetail.stakeTransaction;
      if (time === undefined) {
        time = transaction.time;
      }
      asset = transaction.asset;
      this.calculationServiceLogData(transaction);
      // tslint:disable-next-line:radix max-line-length
      this.calculationServiceLogData('\n\n' + transaction.type + '  : ' + transaction.status + '  :' + new Date(parseInt(transaction.time) * 1000).toLocaleString());
      if (transaction.status !== 'Pending'){

        const assetAmountBN = new BigNumber(transaction.assetamount);
        const targetAmountBN = new BigNumber(transaction.targetamount);
        const poolPrice = txDetail.price;
        const priceInRune = new BigNumber(poolPrice.busdPriceInRune);
        const assetPriceInRune = new BigNumber(poolPrice.assetPriceInRune);
        const runePrinceINUSD = new BigNumber(poolPrice.runePriceInBUSD);
        const assetPriceInUSD = new BigNumber(poolPrice.assetPriceInBUSD);
    //    console.log(poolPrice);

        if ( transaction.type === 'stake'){

          const totalAsset = this.calculateAssetTotal(assetAmountBN, targetAmountBN, priceInRune, assetPriceInRune);
          const totalTarget = this.calculateTargetTotal(assetAmountBN, targetAmountBN, priceInRune, assetPriceInRune);
          const totalUSD = this.calculateUSDTotal(assetAmountBN, targetAmountBN, priceInRune, assetPriceInRune);

          totalAssetStake = totalAssetStake.plus(assetAmountBN);
          totalTargetStake = totalTargetStake.plus(targetAmountBN);
          totalConvertedAsset = totalConvertedAsset.plus(totalAsset);
          totalConvertedTarget = totalConvertedTarget.plus(totalTarget);
          totalConvertedUSD = totalConvertedUSD.plus(totalUSD);

          this.calculationServiceLogData(transaction.pool + ' stake  ASSET: ' + assetAmountBN.div(this.baseNumber).toString()
              + '  RUNE ' + targetAmountBN.div(this.baseNumber).toString() );
          // this.calculationServiceLogData(
          //     'BUSD price in RUNE: ' + priceInRune.toString()
          //     + ' Asset price in  RUNE ' + assetPriceInRune.toString());
          this.calculationServiceLogData(
              'RUNE price in USD: ' + runePrinceINUSD.toString()
              + ' Asset price in  USD ' + assetPriceInUSD.toString());
          this.calculationServiceLogData(
              ' converted to ASSET: ' + totalAsset.div(this.baseNumber).toString()
              + ' converted to  RUNE ' + totalTarget.div(this.baseNumber).toString()
              + ' converted to  USD ' + totalUSD.div(this.baseNumber).toString());
          // tslint:disable-next-line:max-line-length
          this.calculationServiceLogData('\n=> ' + transaction.pool + ' Current balance asset ' + totalAssetStake.div(this.baseNumber).toString()
              + '  RUNE ' + totalTargetStake.div(this.baseNumber).toString() );
          this.calculationServiceLogData(
              ' converted to ASSET: ' + totalConvertedAsset.div(this.baseNumber).toString()
              + ' converted to  RUNE ' + totalConvertedTarget.div(this.baseNumber).toString()
              + ' converted to  USD ' + totalConvertedUSD.div(this.baseNumber).toString());
          this.calculationServiceLogData('\n =>' + asset + ' final Stake Asset ' + totalAssetStake.toString()
              + '   Stake Target ' + totalTargetStake.toString() );
          this.calculationServiceLogData('\n =>' + asset + ' final Stake Asset ' + totalAssetStake.div(this.baseNumber).toString()
              + '   Stake Target ' + totalTargetStake.div(this.baseNumber).toString() );
        } else {

          const assetUnstake = this.calculateAssetTotal(assetAmountBN, targetAmountBN, priceInRune, assetPriceInRune);
          const targetUnstake = this.calculateTargetTotal(assetAmountBN, targetAmountBN, priceInRune, assetPriceInRune);
          const totalUSDUnstake = this.calculateUSDTotal(assetAmountBN, targetAmountBN, priceInRune, assetPriceInRune);

          totalAssetUnstake = totalAssetUnstake.plus(assetAmountBN);
          totalTargetUnstake =  totalTargetUnstake.plus(targetAmountBN);
          totalConvertedAssetUnstake = totalConvertedAssetUnstake.plus(assetUnstake);
          totalConvertedTargetUnstake = totalConvertedTargetUnstake.plus(targetUnstake);
          totalConvertedUSDUnstake = totalConvertedUSDUnstake.plus(totalUSDUnstake);

          this.calculationServiceLogData(transaction.pool + ' Withdraw  ASSET: ' + assetAmountBN.div(this.baseNumber).toString()
              + '  RUNE ' + targetAmountBN.div(this.baseNumber).toString() );
          this.calculationServiceLogData(
              'RUNE price in USD: ' + runePrinceINUSD.toString()
              + ' Asset price in  USD ' + assetPriceInUSD.toString());
          this.calculationServiceLogData(
              ' converted to ASSET: ' + assetUnstake.div(this.baseNumber).toString()
              + ' converted to  RUNE ' + targetUnstake.div(this.baseNumber).toString()
              + ' converted to  USD ' + totalUSDUnstake.div(this.baseNumber).toString());
          this.calculationServiceLogData('\n =>' + asset + ' final Withdraw Asset ' + totalAssetUnstake.toString()
              + '   Withdraw Target ' + totalTargetUnstake.toString() );
          this.calculationServiceLogData('\n =>' + asset + ' final Withdraw Asset ' + totalAssetUnstake.div(this.baseNumber).toString()
              + '   Withdraw Target ' + totalTargetUnstake.div(this.baseNumber).toString() );
        }
      }else {
        this.calculationServiceLogData('Pending transaction is not included in the calculation.');
      }
    }

    this.calculationServiceLogData('\n' + asset + ' final Stake Asset ' + totalAssetStake.div(this.baseNumber).toString()
        + '  final Stake Target ' + totalTargetStake.div(this.baseNumber).toString() );
    this.calculationServiceLogData('\n' + asset + ' final Withdraw Asset ' + totalAssetUnstake.div(this.baseNumber).toString()
        + '  final Withdraw Target ' + totalTargetUnstake.div(this.baseNumber).toString() );
    this.calculationServiceLogData(
        'final converted ASSET: ' + totalConvertedAsset.div(this.baseNumber).toString()
        + 'final converted RUNE ' + totalConvertedTarget.div(this.baseNumber).toString()
        + 'final converted USD ' + totalConvertedUSD.div(this.baseNumber).toString()
       + '\n\n\n');

    this.calculationServiceLogData(
        'final converted unstake ASSET: ' + totalConvertedAssetUnstake.div(this.baseNumber).toString()
        + 'final converted RUNE ' + totalConvertedTargetUnstake.div(this.baseNumber).toString()
        + 'final converted USD ' + totalConvertedUSDUnstake.div(this.baseNumber).toString()
        + '\n\n\n');
    const stakeState: PoolStakeAmount = {
      asset,
      assetAmount: totalAssetStake,
          targetAmount: totalTargetStake,
          totalAssetAmount: totalConvertedAsset,
          totalTargetAmount: totalConvertedTarget,
          totalBUSDAmount: totalConvertedUSD,
    };

    const withdrawState: PoolStakeAmount = {
      asset,
      assetAmount: totalAssetUnstake,
      targetAmount: totalTargetUnstake,
      totalAssetAmount: totalConvertedAssetUnstake,
      totalTargetAmount: totalConvertedTargetUnstake,
      totalBUSDAmount: totalConvertedUSDUnstake,
    };

    const summary: StakePoolSummary = {
      asset,
      stake: stakeState,
      withdraw: withdrawState,
      time
    };
    return summary;

  }

  calculateCurrentOriginalSummary(poolDetail: PoolDetail, originalPoolSummary: StakePoolSummary, price: BigNumber ): StakePoolSummary{
    const priceInRuneBN = new BigNumber(poolDetail.price);
    // tslint:disable-next-line:max-line-length
    const totalUSD = this.calculateUSDTotal(originalPoolSummary.stake.assetAmount, originalPoolSummary.stake.targetAmount, price, priceInRuneBN);
    // tslint:disable-next-line:max-line-length
    const totalUSDWithraw = this.calculateUSDTotal(originalPoolSummary.withdraw.assetAmount, originalPoolSummary.withdraw.targetAmount, price, priceInRuneBN);

    const stake = originalPoolSummary.stake;
    stake.totalBUSDAmount = totalUSD;

    const withdraw = originalPoolSummary.withdraw;
    withdraw.totalBUSDAmount = totalUSDWithraw;

    const summary: StakePoolSummary = {
      asset : originalPoolSummary.asset,
      stake,
      withdraw,
      time : originalPoolSummary.time
    };
    return summary;
  }


  calculatePoolShareSummary(poolDetail: PoolDetail, stakersAssetData: StakersAssetData, price: BigNumber ): StakePoolSummary{
    const stakeUnitsBN = new BigNumber(stakersAssetData.units);
    const priceInRuneBN = new BigNumber(poolDetail.price);
    const poolShare = this.calculateShare(stakeUnitsBN, poolDetail);
    const totalUSD = this.calculateUSDTotal(poolShare[1], poolShare[0], price, priceInRuneBN);
    const totalTarget = this.calculateTargetTotal(poolShare[1], poolShare[0], price, priceInRuneBN);
    const totalAsset = this.calculateAssetTotal(poolShare[1], poolShare[0], price, priceInRuneBN);


    const currentStake: PoolStakeAmount = {
      asset: stakersAssetData.asset,
      assetAmount: poolShare[1],
      targetAmount:  poolShare[0],
      totalAssetAmount: totalAsset,
      totalTargetAmount: totalTarget,
      totalBUSDAmount: totalUSD,
      unit: stakeUnitsBN ,
    };

    const summary: StakePoolSummary = {
      asset: stakersAssetData.asset,
      stake: currentStake
    };
    return summary;

  }

  calculateUSDTotal(amount: BigNumber, targetAmount: BigNumber, runeInBUSDPrice: BigNumber, priceInRune: BigNumber): BigNumber{
    const runePriceINBUSD = runeInBUSDPrice;
    const busdPriceInRUNE = (new BigNumber(1)).div(runePriceINBUSD);
    const priceInASSET = priceInRune.multipliedBy(busdPriceInRUNE);

    const totalTarget =  this.calculateTargetTotal(amount, targetAmount, runeInBUSDPrice, priceInRune);

    const targetPrice = totalTarget.multipliedBy(busdPriceInRUNE);
    // const assetPrice = amount.multipliedBy(priceInASSET);
    // const totalPrice = targetPrice.plus(assetPrice);
    // chage
    return  targetPrice;
  }

  calculateTargetTotal(amount: BigNumber, targetAmount: BigNumber, runeInBUSDPrice: BigNumber, priceInRune: BigNumber): BigNumber{
    const assetPriceInRune = priceInRune;
    this.calculationServiceLogData('assetPriceInRune ' + assetPriceInRune.toFixed(2));
    this.calculationServiceLogData('amount ' + amount);
    this.calculationServiceLogData('targetAmount ' + targetAmount);
    const totalStakePrice = amount.multipliedBy(assetPriceInRune).plus(targetAmount);
    return  new BigNumber(totalStakePrice);
  }

  calculateAssetTotal(amount: BigNumber, targetAmount: BigNumber, runeInBUSDPrice: BigNumber, priceInRune: BigNumber): BigNumber{
    const assetPriceInRune = priceInRune;
    const totalStakePrice = targetAmount.div(assetPriceInRune).plus(amount);
    return  totalStakePrice;
  }


  calculateShare(stakeUnitsBN: BigNumber, eachPool: PoolDetail ): [runeShare: BigNumber, assetShare: BigNumber]{
    const runeDepthBN = new BigNumber(eachPool.runeDepth);
    const assetDepthBN = new BigNumber(eachPool.assetDepth);
    const poolUnitsBN = new BigNumber(eachPool.poolUnits);

    const runeShare = poolUnitsBN
        ? runeDepthBN.multipliedBy(stakeUnitsBN).div(poolUnitsBN)
        : new BigNumber(0);
    const assetShare = poolUnitsBN
        ? assetDepthBN.multipliedBy(stakeUnitsBN).div(poolUnitsBN)
        : new BigNumber(0);

    this.calculationServiceLogData(
        'pool ' + eachPool.asset + '   stakeUnitsBN:' + stakeUnitsBN.div(this.baseNumber).toFormat(2)
        + '   runeDepthBN' + runeDepthBN.div(this.baseNumber).toFormat(2)
        + '   assetDepthBN' + assetDepthBN.div(this.baseNumber).toFormat(2)
        + '   poolUnitsBN' + poolUnitsBN.div(this.baseNumber).toFormat(2)
    );

    return [runeShare, assetShare];
  }

  computeCurrentPercentage(now: StakePoolSummary, current: StakePoolSummary): Profit {
    const currentOriginal = current.stake.totalBUSDAmount;
    const nowUSD = now.stake.totalBUSDAmount.plus(current.withdraw.totalBUSDAmount);
    // const profitAmount = now.stake.totalBUSDAmount.minus(currentOriginal);
    const profitAmount = nowUSD.minus(currentOriginal);
    const percent = (profitAmount.div(currentOriginal)).multipliedBy(100);
    this.calculationServiceLogData('compute percentage ---------------' + current.asset);
    this.calculationServiceLogData('current.stake ' + current.stake.totalBUSDAmount.div(this.baseNumber).toFixed(2));
    this.calculationServiceLogData('current.withdraw ' + current.withdraw.totalBUSDAmount.div(this.baseNumber).toFixed(2));
    this.calculationServiceLogData('currentOriginal ' + currentOriginal.div(this.baseNumber).toFixed(2));
    this.calculationServiceLogData('nowUSD ' + nowUSD.div(this.baseNumber).toFixed(2));
    this.calculationServiceLogData('profitAmount ' + profitAmount.div(this.baseNumber).toFixed(2));
    this.calculationServiceLogData('percent ' + percent);
    this.calculationServiceLogData('compute percentage ---------------');



    let prefix = '+';
    if (percent.isLessThan(0) ){
      prefix = '';
    }
    const profit: Profit = {
      baseTicker: 'BUSD',
      totalPool: new BigNumber(0),
      totalStake: new BigNumber(0),
      totalWithdraw: new BigNumber(0),
      totalGainLoss: profitAmount,
      percentageNumber: percent,
      percentage: prefix + percent.toFixed(2) + '%'
    };
    return profit;
  }

  calculateProfilt(original: StakePoolSummary, current: StakePoolSummary, base: string, baseTicker: string): Profit {
    const baseTickerPool = baseTicker === 'ASSET' ? base : baseTicker;
    switch (baseTicker) {
      case 'BUSD':
        // tslint:disable-next-line:max-line-length
          this.calculationServiceLogData('calculateProfilt');
          this.calculationServiceLogData('final stake ' + (original.stake.totalBUSDAmount).div(this.baseNumber).toFixed(2));
          this.calculationServiceLogData('final withdraw ' + (original.withdraw.totalBUSDAmount).div(this.baseNumber).toFixed(2));
          this.calculationServiceLogData('final ' + (original.stake.totalBUSDAmount.minus(original.withdraw.totalBUSDAmount)).div(this.baseNumber).toFixed(2));
          const profit = this.calculatePercent(original.stake.totalBUSDAmount, current.stake.totalBUSDAmount.plus(original.withdraw.totalBUSDAmount));
          const price: Profit = {
          baseTicker: baseTickerPool,
          totalPool: current.stake.totalBUSDAmount ,
          totalStake: original.stake.totalBUSDAmount,
          totalWithdraw: original.withdraw.totalBUSDAmount,
          totalGainLoss: profit[0],
          percentageNumber: profit[1],
          percentage: profit[2],
        };
          return price;
      case 'RUNE':
        // tslint:disable-next-line:max-line-length
        const profit1 = this.calculatePercent(original.stake.totalTargetAmount, current.stake.totalTargetAmount.plus(original.withdraw.totalTargetAmount));
        const price1: Profit = {
          baseTicker: baseTickerPool,
          totalPool: current.stake.totalTargetAmount ,
          totalStake: original.stake.totalTargetAmount,
          totalWithdraw: original.withdraw.totalTargetAmount,
          totalGainLoss: profit1[0],
          percentageNumber: profit1[1],
          percentage: profit1[2],
        };
        return price1;
      case 'ASSET':
        // tslint:disable-next-line:max-line-length
        const profit2 = this.calculatePercent(original.stake.totalAssetAmount, current.stake.totalAssetAmount.plus(original.withdraw.totalAssetAmount));
        const price2: Profit = {
          baseTicker: baseTickerPool,
          totalPool: current.stake.totalAssetAmount ,
          totalStake: original.stake.totalAssetAmount,
          totalWithdraw: original.withdraw.totalAssetAmount,
          totalGainLoss: profit2[0],
          percentageNumber: profit2[1],
          percentage: profit2[2],
        };
        return price2;
    }
  }

  calculatePercent(original: BigNumber, current: BigNumber): [BigNumber, BigNumber, string]{
    const percent = (current.minus(original)).div(original);
    let prefix = '';
    if (percent.isGreaterThan(0)){
      prefix = '+';
    }
    // console.log(' original amount: ' + original.div(this.baseNumber).toString()
    //     + 'current share: ' + current.div(this.baseNumber).toString());
    // console.log('profit: in ' + base + ' ' + percent + ' = ' + percent.multipliedBy(100) + '%');
    return [current.minus(original), percent, prefix + percent.multipliedBy(100).toFixed(2) + '%'];
  }

}
