import { TestBed } from '@angular/core/testing';

import { UserPoolsService } from './user-pools.service';

describe('UserPoolsService', () => {
  let service: UserPoolsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserPoolsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
