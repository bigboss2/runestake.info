import { Injectable } from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import BigNumber from 'bignumber.js';
import {WeeklyState} from '../models/Asgard-consumer-model/WeeklyStateList';
import {PoolDetail} from '../models/poolDetail';
import {AsgardConsumerApiService} from './asgard-consumer-api.service';
import {CalculationService} from './calculation.service';
import {environment} from '../../environments/environment';
import {UtilsService} from './utils.service';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(private utils: UtilsService) { }
  asgardApiService: AsgardConsumerApiService;
  calculationService: CalculationService;
  weeklyState: { [pool: string]: WeeklyState[] } = {};
  assetColor: string[] = [];

   lineChartData: ChartDataSets[] = [];
   lineChartLabels: Label[] = [];
   lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    tooltips: {
      callbacks: {
        // label: (item, data) => '' + item.datasetIndex + ' ' + item.index,
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          display: true
        }
      ]
    },
    annotation: {
    },
  };
  public lineChartColors: Color[] = [];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  demoLogData(data){
    if (!environment.production){
      console.log(data);
    }
  }

  loadWeeklyState(address, poolList, baseTicker: string, assetColor: string[], isReset: boolean){
    this.assetColor = assetColor;
    this.asgardApiService.getAllWeeklyState(address, poolList, isReset).subscribe(result => {
      this.demoLogData(result);
      this.weeklyState = result;
      this.reloadWeeklyChart(baseTicker);
    }, error => {
      this.demoLogData(error);
    });
  }

  reloadWeeklyChart( baseTicker: string){
    const dataset: ChartDataSets[] = [];
    let date: Label[] = [];
    let index = 0;
    for (const [key, val] of Object.entries(this.weeklyState)) {
      const graphData: number[] = [];
      const tempDate: Label[] = [];

      val.forEach(value => {
            this.demoLogData(value);
            this.demoLogData('price ' + new BigNumber(value.pricerune).toNumber().toFixed(2));
            graphData.push(this.calculateWeeklyState(value, key , baseTicker));
            tempDate.push(value.day);
          }
      );

      let suffix = '';
      if (baseTicker === 'BUSD'){
        suffix = ': $';
      }else if (baseTicker === 'ASSET'){
        suffix = '';
      }else {
        suffix = ': ᚱ';
      }

      const chart: ChartDataSets = {
        data: graphData,
        label: this.utils.getAssetName(key) + ' ' + suffix,
        pointRadius: 0,
        backgroundColor: this.convertHex(this.assetColor[index + 1], 30),
        borderColor: this.assetColor[index + 1],
        borderWidth: 1
      };
      date = tempDate;
      dataset.push(chart);
      index = index + 1;
    }
    this.demoLogData('dataset');
    this.demoLogData(dataset);
    this.lineChartData = dataset;
    this.lineChartLabels = date;
  }

  calculateWeeklyState(states: WeeklyState, pool: string,  baseTicker: string): number {
    const poolDetail: PoolDetail = {
      runeDepth:  `${states.runedepth}`,
      assetDepth: `${states.assetdepth}`,
      poolUnits: `${states.poolunits}`
    };
    const poolShare = this.calculationService.calculateShare(new BigNumber(states.stakeunit), poolDetail);
    const priceRune = new BigNumber(states.pricerune);
    const busdPriceRune = new BigNumber(states.busdpricerune);
    this.demoLogData('asset ' + poolShare[1].toFixed(3));
    this.demoLogData('rune ' + poolShare[0].toFixed(3));
    this.demoLogData('price rune :' + priceRune);
    let totalRune = new BigNumber(0);
    switch (baseTicker) {
      case 'BUSD':
        totalRune = this.calculationService.calculateUSDTotal(poolShare[1], poolShare[0], busdPriceRune, priceRune );
        break;
      case 'ASSET':
        totalRune = this.calculationService.calculateAssetTotal(poolShare[1], poolShare[0], busdPriceRune, priceRune );
        break;
      case 'RUNE' :
        totalRune = this.calculationService.calculateTargetTotal(poolShare[1], poolShare[0], busdPriceRune, priceRune );
        break;
    }
    if (baseTicker === 'ASSET'){
      return parseFloat(totalRune.div(environment.BNB_BASE_NUMBER).toNumber().toFixed(5));
    }else {
      return parseFloat(totalRune.div(environment.BNB_BASE_NUMBER).toNumber().toFixed(2));
    }

  }

  convertHex(hex: string, opacity: number){
    hex = hex.replace('#', '');
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);

    const result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
    return result;
  }

}
