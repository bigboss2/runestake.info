import { TestBed } from '@angular/core/testing';

import { AsgardConsumerApiService } from './asgard-consumer-api.service';

describe('AsgardConsumerApiService', () => {
  let service: AsgardConsumerApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsgardConsumerApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
