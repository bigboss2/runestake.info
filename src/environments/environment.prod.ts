export const environment = {
  production: true,
  ASGARD_BASE_URL: 'https://asgard-consumer.vercel.app',
  BNB_BASE_NUMBER:  Math.pow(10, 8) // 1e8
};
